#!/usr/bin/env groovy

pipeline {
    agent any
    tools {
        gradle 'gradle'
    }
    environment {
        DOCKER_REGISTRY="docker.ehsan.cf"
        K8S_DEPLOYMENT_NAME="demo"
        CHART_NAME="demo"
        REPO_NAME="nexus"
        K8S_PROD_NAMESPACE="prod"
        K8S_CONFIG="/opt/kubeconfig/config"
        HELM_REPO="https://nexus.ehsan.cf/repository/helm-hosted/"
    }
    stages {
        stage('increment version') {
            steps {
                script {
                    def nextVersionFromGit(scope) {
                        def latestVersion = sh(returnStdout: true, script: 'git describe --tags --abbrev=0 --match *.*.* 2> /dev/null || echo 0.0.0').trim()
                        def (major, minor, patch) = latestVersion.tokenize('.').collect { it.toInteger() }
                        def nextVersion
                        switch (scope) {
                            case 'major':
                                nextVersion = "${major + 1}.0.0"
                                break
                            case 'minor':
                                nextVersion = "${major}.${minor + 1}.0"
                                break
                            case 'patch':
                                nextVersion = "${major}.${minor}.${patch + 1}"
                                break
                        }
                        nextVersion
                    }
                }
            }
        }

        stage('build app') {
            steps {
                script {
                    echo "building the application..."
                    sh 'gradlew build'
                }
            }
        }
    }
}